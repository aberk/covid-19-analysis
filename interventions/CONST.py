import os

_HOME = os.path.expanduser("~") + os.path.sep

uname = os.uname()
nodename = uname.nodename


def _on_aarons_machine():
    return nodename == "grimpoteuthis"


def get_dbdir():
    if _on_aarons_machine():
        DB_DIR = (
            _HOME + "Dropbox/school/phd/research/covid-19-us-interventions/"
        )
    else:
        raise NotImplementedError(
            "Other machines haven't yet joined the cause."
        )
    return DB_DIR


def get_data_dir():
    if _on_aarons_machine():
        return _HOME + "data/covid/"
    raise NotImplementedError("Other machines haven't yet joined the cause.")
