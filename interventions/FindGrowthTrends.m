clear
figure(1)
clf


% import the JHU data
cd /Users/cytryn/Dropbox/Research/COVID/data
a=csvread('JHUConfirmedCases20200325.csv');
location=readtable('JHUCountries20200325.csv');

% extract the time series (each row is a country, first two entries are its
% GPS coordinates(?) and the rest are confirmed case numbers by day
b=a(:,3:end);

N=10; % find exp fits for the first/last N time points

c=b.*(b>=100);  % zero out all data below 100

% reorganize data so that all rows start at the first date with > 100 cases
for j=1:size(c,1)               % run through countries one by one
    indPos=find(c(j,:));        % find indices of non-zero entries
    indZero=find(c(j,:)==0);    % find indices of zero entries
    c(j,:)=[c(j,indPos) c(j,indZero)];  % move zeros to the end of each row
end

% for j=1:size(c,1)               % run through countries one by one
%     C=c(j,find(c(j,:)));        % make vector of the non-zero entries for plotting
%     plot(log(C),'b','linewidth',2)  % log plot (base e)
% end

for j=1:size(c,1)               % run through countries one by one
    C=c(j,find(c(j,:)));        % make vector of just the non-zero entries
    
    L=length(C);
    
    if L>25
        h=[];
        clf
        hold on

        plot(log(C),'b','linewidth',2)
        
        D=C(1:min(N,L));          % Make vector from the first N entries of C
        if ~isempty(D)
            h(1)=plot(log(D),'r','linewidth',2);            % log plot (base e)
            A=[ones(length(D),1) [1:length(D)]'];   % form matrix for pseudo-inverse calc of least squares fit
            B=log(D');                  % RHS of Ax=B equation
            x=(transpose(A)*A)\transpose(A)*B; % least squares fit coefficients
            y(j,1)=x(2);                % save the exponential coefficient (i.e. the k in exp(kt))
            h(2)=plot([1 min(N,L)],x(1)+x(2)*[1 min(N,L)],'g','linewidth',2);
        end

        D=C(max(L-N+1,1):L);
        if ~isempty(D)
            h(3)=plot(max(length(C)-N+1,1):length(C),log(D),'r','linewidth',2);                    % log plot (base e)
            A=[ones(length(D),1) [1:length(D)]'];   % form matrix for pseudo-inverse calc of least squares fit
            B=log(D');                      % RHS of Ax=B equation
            x=(transpose(A)*A)\transpose(A)*B; % least squares fit coefficients
            y(j,2)=x(2);                    % save the exponential coefficient (i.e. the k in exp(kt))
            h(4)=plot([length(C)-N+1 length(C)],x(1)+x(2)*[length(C)-N+1 length(C)],'g','linewidth',2);
        end
        text(15,4.8,location.Country(j))
        text(15,5,location.State(j))
        
        pause(3)
        if ~isempty(h)
            delete(h)
        end
        
    end
end
    

