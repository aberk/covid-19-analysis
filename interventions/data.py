import os
import pandas as pd

HOME = os.path.expanduser("~") + os.path.sep
DATA_DIR = HOME + "data/covid/"
JHU_DIR = DATA_DIR + "COVID-19/csse_covid_19_data/csse_covid_19_time_series/"
NYT_DIR = DATA_DIR + "covid-19-data/"


def process_jhu_df(
    raw_df, value_name="confirmed_cases", size_threshold=100, obs_threshold=10
):

    # Keep only the date columns and the state name
    # Turn that dataframe into long format and parse the date column as a pd
    # datetime object
    # Sort everything by date so that its correctly ordered.
    df_melt = (
        raw_df.filter(regex="([0-9]+/[0-9]+/[0-9]+|Province_State)")
        .pivot_table(index="Province_State", aggfunc="sum")
        .reset_index()
        .melt(id_vars="Province_State", var_name="Date", value_name=value_name)
        .assign(Date=lambda df: pd.to_datetime(df.Date))
        .sort_values("Date")
        .reset_index(drop=True)
    )

    # Determine how many events/cases each state has.
    max_stats = df_melt.groupby("Province_State")[value_name].max()
    states_over_thresh = max_stats.loc[
        max_stats >= size_threshold
    ].reset_index()

    # Keep only states with sufficiently many events (e.g., 100 cases or 10 deaths)
    # And filter out all data points from before the state reached that event threshold
    df_filt = (
        df_melt.loc[
            df_melt.Province_State.isin(
                states_over_thresh.Province_State.values
            )
        ]
        .groupby("Province_State")
        .apply(
            lambda group: group.loc[
                group.index >= (group[value_name] >= size_threshold).idxmax()
            ]
        )
        .reset_index(drop=True)
    )

    # Compute shifted_day (e.g., day since 100th case, or day since 10th death)
    df_filt = df_filt.assign(
        shifted_day=lambda df: df.groupby("Province_State")
        .apply(lambda group: group.Date - group.Date.min())
        .reset_index(drop=True)
    )

    # drop states with fewer than 10 observations
    num_observations = df_filt.groupby("Province_State").apply(
        lambda group: group.shape[0]
    )
    states_with_suff_data = num_observations.loc[
        num_observations >= obs_threshold
    ].reset_index()
    df_filt = df_filt.loc[
        df_filt.Province_State.isin(states_with_suff_data.Province_State.values)
    ]
    return df_filt


def _load_jhu_us_state_data(obs_threshold=10):
    confirmed_fname = "time_series_covid19_confirmed_US.csv"
    deaths_fname = "time_series_covid19_deaths_US.csv"
    confirmed_fpath = JHU_DIR + confirmed_fname
    deaths_fpath = JHU_DIR + deaths_fname
    confirmed = pd.read_csv(confirmed_fpath)
    confirmed = process_jhu_df(
        confirmed,
        value_name="confirmed_cases",
        size_threshold=100,
        obs_threshold=obs_threshold,
    )
    deaths = pd.read_csv(deaths_fpath)
    deaths = process_jhu_df(
        deaths,
        value_name="deaths",
        size_threshold=10,
        obs_threshold=obs_threshold,
    )
    return confirmed, deaths


def load_us_state_data(load_jhu=True, obs_threshold=10):
    if load_jhu:
        confirmed, deaths = _load_jhu_us_state_data(obs_threshold)
    else:
        raise NotImplementedError(
            "Loading of the NYTimes data has not yet been implemented."
        )
    return confirmed, deaths


def subset_by_state(df, state="New York"):
    return df.loc[df.Province_State == state]
