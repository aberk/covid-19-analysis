"""
download_script

Script to get the latest version of the NYTimes intervention data. Also
generates plots for the day.

Author: Aaron Berk <aberk@math.ubc.ca>
Copyright © 2020, Aaron Berk, all rights reserved.
Created:  6 April 2020
"""

import os
from datetime import datetime
import matplotlib.pyplot as plt

from CONST import get_dbdir
from data import load_us_state_data
from interventions import load_formatted_us_intervention_data
from model import (
    fit_models_on_thirds,
    get_shifted_day_for_interventions,
    predict_on_shifted_day,
    print_lm_summary,
)
from viz import add_ordinance_vlines_to_axes, plot_pred_on_thirds


FIG_DIR = os.path.join(get_dbdir(), "fig/")


def main():
    """
    main()

    We only consider States with at least 10 observations since reaching 100
    cases / 10 deaths.
    """
    print("download_script.py")
    print(datetime.now().ctime())

    print(
        "\nDownloads latest intervention data from NYTimes website and saves "
        "the new figures to the Dropbox folder."
    )

    # Try to get new intervention data from NYTimes
    interv_df = load_formatted_us_intervention_data(False, True)

    # Load confirmed and deaths data
    # (size_threshold = 100 & 10, obs_threshold = 10)
    confirmed, deaths = load_us_state_data()

    # most recent date for cases and deaths data
    cases_max_date = confirmed.Date.max().date().isoformat()
    deaths_max_date = deaths.Date.max().date().isoformat()

    # get the models for the first thirds and final thirds of the case data
    lm_dict_confirmed, coef_confirmed = fit_models_on_thirds(
        confirmed, verbose=False
    )
    # get the models for the first thirds and final thirds of the deaths data
    lm_dict_deaths, coef_deaths = fit_models_on_thirds(
        deaths, "deaths", verbose=False
    )
    # print a summary and save the coefficients to a CSV file
    lm_summary = print_lm_summary(coef_confirmed, coef_deaths, partial=False)
    lm_summary.to_csv(
        os.path.join(
            get_dbdir(),
            f"data/lm_coef_summary_cases_{cases_max_date}_deaths_{deaths_max_date}.csv",
        )
    )
    print(lm_summary.dropna())

    # Get the predictions from the linear models so that we can visualize the fits
    confirmed2 = predict_on_shifted_day(confirmed, lm_dict_confirmed)
    deaths2 = predict_on_shifted_day(deaths, lm_dict_deaths)

    # Load in the interventions data for vline annotations
    interv_df = load_formatted_us_intervention_data()
    interv_df_death_shift = get_shifted_day_for_interventions(interv_df, deaths)
    interv_df_case_shift = get_shifted_day_for_interventions(
        interv_df, confirmed
    )

    # Make the visualization for the cases data
    fig, axx = plot_pred_on_thirds(confirmed2, use_subplots=True)

    add_ordinance_vlines_to_axes(axx, interv_df_case_shift, confirmed)
    plt.tight_layout()
    fig.savefig(
        os.path.join(
            FIG_DIR,
            f"confirmed_cases_trends_vs_shifted_day_{cases_max_date}.pdf",
        ),
        dpi=300,
    )

    # Make the visualization for the deaths data
    fig, axx = plot_pred_on_thirds(deaths2, "deaths", use_subplots=True)
    add_ordinance_vlines_to_axes(
        axx, interv_df_death_shift, events_df=deaths, event_name="deaths"
    )
    plt.tight_layout()
    fig.savefig(
        os.path.join(
            FIG_DIR,
            f"cumulative_mortality_trends_vs_shifted_day_{deaths_max_date}.pdf",
        ),
        dpi=300,
    )
    return


if __name__ == "__main__":
    plt.rcParams["lines.linewidth"] = 3
    plt.rcParams["font.size"] = 16
    plt.rcParams["axes.labelsize"] = 16
    main()

# # download_script.py ends here
