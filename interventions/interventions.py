"""
parse_interventions

This code will parse the us-interventions-by-county.txt file and create
something "usable" out of it.

Author: Aaron Berk <aberk@math.ubc.ca>
Copyright © 2020, Aaron Berk, all rights reserved.
Created: 30 March 2020

Commentary:
   In collaboration with Eric Cytrynbaum
"""
import os
from glob import glob
import re
from datetime import datetime
import pandas as pd
from bs4 import BeautifulSoup
import requests
import cmd
from csv import writer


# set home_dir to ~/
_HOME = os.path.expanduser("~") + os.path.sep
_TODAY = datetime.today().strftime("%Y%m%d")
_BASE_DIR = _HOME + "data/covid/"
_CSV_TAG = "us-interventions-by-county"
_HTML_TAG = "nytimes_intervention_data"
_ACCESSED_ = "_accessed_{DATE}"
_ACCESSED = _ACCESSED_.format(DATE=_TODAY)
_DEFAULT_CSV_PATH = _BASE_DIR + _CSV_TAG + _ACCESSED + ".csv"
_DEFAULT_HTML_PATH = _BASE_DIR + _HTML_TAG + _ACCESSED + ".html"
_META_DATA_FP = _BASE_DIR + "meta_nytimes_intervention_data.csv"


def _pprint(list_of_strings, displaywidth=80):
    cli = cmd.Cmd()
    cli.columnize(list_of_strings, displaywidth=displaywidth)
    return


def _search_intervention_file(
    directory=None, file_name=None, date_tag=None, html_or_csv=None
):
    if (html_or_csv is None) or (html_or_csv.lower() == "html"):
        if directory is None:
            directory = _BASE_DIR
        if file_name is None:
            file_name = _HTML_TAG
        if date_tag is None:
            date_tag = "*"
        extension = ".html"
    else:
        if directory is None:
            directory = _BASE_DIR
        if file_name is None:
            file_name = _CSV_TAG
        if date_tag is None:
            date_tag = "*"
        extension = ".csv"

    file_path = os.path.join(directory, file_name + date_tag + extension)
    results = sorted(glob(file_path))
    if len(results) == 0:
        print(
            f"Warning: returning None. No file found matching"
            f"\n  {file_path}"
        )
        return
    matched_file_path = results[-1]
    print(f"Found file matching pattern:\n  {matched_file_path}")
    return matched_file_path


def _search_by_file_path(file_path=None, html_or_csv=None):
    if file_path is None:
        return _search_intervention_file(html_or_csv=html_or_csv)
    else:
        assert isinstance(
            file_path, str
        ), f"Expected string for file_path but got\n  {file_path}"
    directory, file_name = os.path.split(file_path)
    base_name, extension = os.path.splitext(file_name)
    return _search_intervention_file(
        directory=directory, file_name=base_name, html_or_csv=html_or_csv
    )


def _get_access_date(file_path):
    base_name = os.path.splitext(os.path.split(file_path)[1])[0]
    return re.search("[0-9]{8}", base_name).group()


def download_us_intervention_data_from_nytimes(url=None, save_path=None):
    """
    download_us_intervention_data_from_nytimes(url=None, save_path=None)

    Saves HTML data to a file if file_path does not already exist. Returns None.

    Inputs
    ------
    url : string
        Default: "https://www.nytimes.com/interactive/2020/us/coronavirus-stay-at-home-order.html"
    save_path : string
        Default: "~/data/covid/nytimes_intervention_data_accessed_{DATE}.html"

    """
    nytimes_interv_url = (
        "https://www.nytimes.com/interactive/2020/us/"
        "coronavirus-stay-at-home-order.html"
    )
    if url is None:
        url = nytimes_interv_url
    if save_path is None:
        save_path = _DEFAULT_HTML_PATH
    if isinstance(save_path, str) and os.path.exists(save_path):
        print(f"Warning: file {save_path} exists. Refusing to overwrite.")
        return

    nytimes_interv_response = requests.get(url)
    if not nytimes_interv_response.status_code == 200:
        print(
            f"Unsuccessful: status code is {nytimes_interv_response.status_code}"
        )
    nytimes_interv_src = nytimes_interv_response.text
    with open(save_path, "w") as fp:
        fp.writelines(nytimes_interv_src)
        print(
            f"Wrote html source for NYTimes county-level Intervention data to:"
            f"\n  {save_path}"
        )
    return


def _append_html_source_metadata(soup, file_path, date_accessed=None):
    meta_modified = soup.find("meta", attrs={"property": "article:modified"})
    meta_published = soup.find("meta", attrs={"property": "article:published"})
    date_modified = meta_modified.attrs["content"]
    date_published = meta_published.attrs["content"]
    if date_accessed is None:
        date_accessed = _get_access_date(file_path)
    directory, file_name = os.path.split(file_path)

    df = pd.read_csv(_META_DATA_FP)
    if file_name in df.file_name:
        print("returning")
        return
    row = (directory, file_name, date_accessed, date_modified, date_published)
    with open(_META_DATA_FP, "a+") as fp:
        csv_writer = writer(fp)
        csv_writer.writerow(row)
    return


def load_nytimes_intervention_html_source(download=False, file_path=None):
    """
    load_nytimes_intervention_html_source(download=False, file_path=None)

    Loads the NYTimes Intervention HTML file. If download is True, the most recent
    version is fetched from
    https://www.nytimes.com/interactive/2020/us/coronavirus-stay-at-home-order.html
    and is saved to file_path. Otherwise, this function attempts to load the
    saved version from file_path. The default location for this file is:
        "~/data/covid/nytimes_intervention_data_accessed_{DATE}.html"
    which can be changed by passing a string argument for file_path.

    Inputs
    ------
    download : bool
        Default: False
    file_path : str
        Default: "~/data/covid/nytimes_intervention_data_accessed_{DATE}.html"

    Outputs
    -------
    soup : BeautifulSoup object
    """
    date_accessed = None
    if file_path is not None:
        assert isinstance(
            file_path, str
        ), f"Expected string but got {file_path}"
    if download:
        if file_path is None:
            file_path = _DEFAULT_HTML_PATH
        print(f"Downloading HTML data to:\n  {file_path}")
        download_us_intervention_data_from_nytimes(save_path=file_path)
        date_accessed = datetime.now().isoformat()
    elif file_path is None:
        file_path = _search_by_file_path(file_path, html_or_csv="html")
    print(f"Reading HTML data from:\n  {file_path}")
    with open(file_path, "r") as fp:
        nytimes_interv_src = "\n".join(fp.readlines())
    soup = BeautifulSoup(nytimes_interv_src, features="lxml")
    _append_html_source_metadata(soup, file_path, date_accessed)
    return soup


def _list_states_with_available_data(state_info):
    """
    _list_states_with_available_data(state_info)

    Lists the names of states that have data associated to them.

    Input
    -----
    state_info: list
        List of elements as created using BeautifulSoup. The elements should
        have an h3 tag with a stripped_strings variable whose first element is
        the State's name.

    Output
    ------
    states_with_available_data : list
        List of strings corresponding to States' names.
    """
    states_with_available_data = [
        next(state.find("h3").stripped_strings) for state in state_info
    ]
    return states_with_available_data


def _get_state_name(state):
    """
    _get_state_name(state)

    Returns the state's name by searching the stripped_strings of the first h3
    element within the State's main div tag.
    """
    return next(state.find("h3").stripped_strings)


def _get_state_code(state):
    """
    _get_state_code(state)

    Returns the code for the state by searching a class attribute of the state's
    div tag. (e.g., "AK" for Alaska, "FL" for Florida, etc.)
    """
    return state.get("data-state")


def _parse_population_text(pop_text):
    """
    _parse_population_text(pop_text)

    Turns the population string into an integer (e.g., "About 2 million people"
    becomes 2e6 and "About 702,000 people" becomes 702000).
    """
    pop_text = pop_text.split("About ")[1].split(" people")[0]
    pop_text = "".join(pop_text.split(","))
    if "million" in pop_text:
        pop_value = float(pop_text.split()[0]) * 1e6
    else:
        pop_value = float(pop_text)
    pop_value = int(pop_value)
    return pop_value


def _get_population(state_or_place):
    """
    _get_population(state_or_place)

    Retrieve the population (as an integer) from a State's or Place's main div.
    """
    return _parse_population_text(
        state_or_place.find("span", attrs={"class": "l-population"}).text
    )


def _is_state_wide(state):
    """
    _is_state_wide(state)

    Determine whether the intervention for the State was implemented at
    State-level or by county/city/region.

    Note: the two cases must be handled in different ways: not only is the HTML
    layout is different but county-level interventions affect differing numbers
    of people, could have been implemented on different days, and could admit
    differences in the ordinance specification.
    """
    return "statewide" in state.get("class")


def _parse_order_date(order_date_string):
    """
    _parse_order_date(order_date_string)

    A hacky function to get a datetime object from a string. So far it has been
    sufficient to consider 3 particular cases and use some mild regexing. But if
    something breaks, I wouldn't be surprised if it's here.

    Input
    -----
    order_date_string : str
        Any of the following formats:
          * Mar 28
          * Mar 28 at 5 p.m.
          * Mar 28 at 5:31 p.m.
          * Mar 28 at 5 P.M
          * Mar 28 at 5:31 PM

    Output
    ------
    datetime_object : datetime
    """
    if len(order_date_string.split()) == 2:
        dt_obj = datetime.strptime("2020 " + order_date_string, "%Y %B %d")
    elif ":" not in order_date_string:
        order_date_string = "2020 " + re.sub(
            r"p\.m\.", "PM", re.sub(r"a\.m\.", "AM", order_date_string)
        )
        dt_obj = datetime.strptime(order_date_string, "%Y %B %d at %H %p")
    else:
        order_date_string = "2020 " + re.sub(
            r"p\.?m\.?", "PM", re.sub(r"a\.?m\.?", "AM", order_date_string)
        )
        dt_obj = datetime.strptime(order_date_string, "%Y %B %d at %H:%M %p")
    return dt_obj


def _get_l_order_statewide(state):
    """
    _get_l_order_statewide(state)

    Retrieves the ordinance information for a state if the ordinance was
    state-wide. In this case, it returns the type (e.g. "Shelter in Place") and
    date on which the ordinance became effective (e.g. as a datetime object).
    """
    if _is_state_wide(state):
        l_order = list(
            state.find("p", attrs={"class": "l-order"}).stripped_strings
        )
        order_type = l_order[0]
        effective_on = _parse_order_date(l_order[1].split("effective ")[1])
        return order_type, effective_on
    else:
        print(
            f"order(s) for state {_get_state_name(state)} is not"
            f" state-wide. Returning None."
        )
    return


def get_statewide_info_df(state_info):
    """
    get_statewide_info_df(state_info)

    Returns a dataframe containing the data for all States for which state-wide
    ordinances were implemented.

    Input
    -----
    state_info: list
        A list of div objects from BeautifulSoup.

    Output
    ------
    statewide_info_df : pd.DataFrame
        With columns: state_name, state_code, population, order_type,
                      effective_on
    """

    state_names = []
    state_codes = []
    state_populns = []
    order_types = []
    order_dates = []

    for state in state_info:
        if not _is_state_wide(state):
            continue

        state_names.append(_get_state_name(state))
        state_codes.append(_get_state_code(state))
        state_populns.append(_get_population(state))
        otype, odate = _get_l_order_statewide(state)
        order_types.append(otype)
        order_dates.append(odate)

    df_statewide_info = pd.DataFrame(
        {
            "state_name": state_names,
            "state_code": state_codes,
            "population": state_populns,
            "order_type": order_types,
            "effective_on": order_dates,
        }
    )
    return df_statewide_info


def _get_places_from_non_statewide(state):
    """
    _get_places_from_non_statewide(state)

    For a state which did not implement a state-wide ordinance, this function
    retrieves the divs corresponding to the places (e.g., counties, cities,
    regions) that implemented their own ordinances.
    """
    if not _is_state_wide(state):
        places = state.find_all("div", attrs={"class": "place-wrap"})
        return places
    else:
        print(
            f"Expected non-statewide state but got "
            f"{_get_state_name(state)}. Returning None."
        )
    return


def _get_place_name(place):
    return next(place.find("p", attrs={"class": "l-place"}).stripped_strings)


def _get_place_order_type(place):
    return next(place.find("p", attrs={"class": "l-order"}).stripped_strings)


def _get_place_order_date(place):
    return _parse_order_date(
        next(
            place.find("span", attrs={"class": "l-date"}).stripped_strings
        ).split("effective ")[1]
    )


def _parse_places_for_state(state):
    """
    _parse_places_for_state(state)

    Returns a dataframe containing information for each place within the State
    that has implemented an ordinance.

    Output
    ------
    places_info_df : pd.DataFrame
        Containing columns: place_name, population, order_type, effective_on,
                            state_name, state_code
    """
    if _is_state_wide(state):
        print(
            "Expected non-statewide state but got "
            f"{_get_state_name(state)}. Returning None."
        )
        return

    state_name = _get_state_name(state)
    state_code = _get_state_code(state)
    places = _get_places_from_non_statewide(state)

    names = []
    populations = []
    order_types = []
    order_dates = []

    for place in places:
        names.append(_get_place_name(place))
        populations.append(_get_population(place))
        order_types.append(_get_place_order_type(place))
        order_dates.append(_get_place_order_date(place))

    places_info_df = pd.DataFrame(
        {
            "place_name": names,
            "population": populations,
            "order_type": order_types,
            "effective_on": order_dates,
        }
    )
    places_info_df["state_name"] = state_name
    places_info_df["state_code"] = state_code
    return places_info_df


def get_non_statewide_info_df(state_info):
    """
    get_non_statewide_info_df(state_info)

    Aggregates places info for each state that has not implemented state-wide
    ordinances. Returns it as a dataframe.

    Outputs
    non_statewide_info_df : pd.DataFrame
        Contains columns: place_name, population, order_type, effective_on,
                          state_name, state_code
    """
    nsw_info_df = [
        _parse_places_for_state(state)
        for state in state_info
        if not _is_state_wide(state)
    ]
    nsw_info_df = pd.concat(nsw_info_df)
    return nsw_info_df


def parse_intervention_soup(soup, csv_file_path=None, verbose=True):
    """
    parse_intervention_soup(soup, csv_file_path=None, verbose=True)

    Turns the soup object containing the HTML data into a dataframe containing
    the US intervention information created/maintained by the New York Times.

    Inputs
    ------
    soup
    csv_file_path : bool
        Default: None
    verbose : bool
        Default: True

    Outputs
    -------
    interv_df : pd.DataFrame
        Contains columns: state_name, state_code, population, order_type,
                          effective_on, place_name, population
    """
    main_content = soup.find(name="div", attrs={"class": "list-wrap"})
    state_info = [
        child
        for child in main_content.children
        if (not isinstance(child, str)) and child.has_attr("class")
    ]
    states_with_available_data = _list_states_with_available_data(state_info)
    if verbose:
        print("\nStates with available data:")
        _pprint(states_with_available_data)
    sw_info_df = get_statewide_info_df(state_info)
    sw_info_df["state_wide"] = True
    nsw_info_df = get_non_statewide_info_df(state_info)
    nsw_info_df["state_wide"] = False
    interv_df = pd.concat((sw_info_df, nsw_info_df)).reset_index(drop=True)
    interv_df["effective_on"] = pd.to_datetime(interv_df.effective_on)

    if verbose:
        num_state_wide = interv_df.state_wide.sum()
        num_non_state_wide = nsw_info_df.state_name.unique().size
        print(f"\nState-wide ordinances ({num_state_wide})")
        _pprint(sw_info_df.state_name.unique().tolist())
        print(f"\nStates with regional ordinances ({num_non_state_wide})")
        _pprint(nsw_info_df.state_name.unique().tolist())
        print()

    if (csv_file_path is not None) and isinstance(csv_file_path, str):
        if os.path.exists(csv_file_path):
            print(
                f"Warning: CSV already exists. Refusing to overwrite:\n  {csv_file_path}"
            )
        else:
            print(f"Saving US intervention data to CSV:\n  {csv_file_path}")
            interv_df.to_csv(csv_file_path)
    return interv_df


def load_formatted_us_intervention_data(
    load_saved_csv=True,
    download_html=False,
    html_file_path=None,
    csv_file_path=None,
    verbose=True,
):
    """
    load_formatted_us_intervention_data(
        load_saved_csv=True,
        download_html=False,
        html_file_path=None,
        csv_file_path=None,
        verbose=True,
    )

    Loads the US Intervention CSV, as created/published/maintained by the New
    York Times. For the article version of this information, see:
    https://www.nytimes.com/interactive/2020/us/coronavirus-stay-at-home-order.html

    By default, this function attempts to load the file from local, using the
    default path:
        "~/data/covid/us-interventions-by-county_accessed_{DATE}.csv"

    The argument download_html is only used if load_saved_csv is False or
    unsuccessful. In that case, download_html determines if the HTML page from
    NYTimes will be re-scraped and saved to html_file_path, or if the HTML page
    will be loaded from local memory. If download is True, then the HTML file
    will be scraped from the URL above and saved to html_file_path, the default
    value for which is:
        "~/data/covid/nytimes_intervention_data_accessed_{DATE}.html"
    If download_html is False, the function attempts to load the
    HTML data from this path instead.

    Once the HTML data is loaded (from memory or the internet), the US
    intervention CSV is generated. This data is returned as a dataframe after
    being saved to csv_file_path.


    Inputs
    ------
    load_saved_csv : bool
        Whether to try loading the CSV from local memory (gracefully fails by
        attempting to create the data if no file is found). Default: True
    download_html : bool
        Whether to download the HTML data from the New York Times URL.
        Default: False
    html_file_path : str
        The file path for the HTML file that is downloaded from the New York
        Times URL. Default: None
    csv_file_path : str
        The name of the csv file to load (or to create if the CSV file does not
        exist). Default: None
    verbose : bool
        Whether to print verbose output about available data. Default: True

    """
    if load_saved_csv:
        csv_file_path = _search_by_file_path(csv_file_path, html_or_csv="csv")
        if not os.path.exists(csv_file_path):
            print(f"Warning: CSV file not found:\n  {csv_file_path}")
            load_saved_csv = False
    if load_saved_csv:
        print(f"Loading CSV file from:\n  {csv_file_path}")
        interv_df = pd.read_csv(
            csv_file_path, index_col=0, parse_dates=["effective_on"]
        )
        return interv_df
    else:
        print(f"Creating dataframe from HTML data.")
        if html_file_path is None:
            html_file_path = _DEFAULT_HTML_PATH
        html_file_path = _search_by_file_path(html_file_path, "html")
        soup = load_nytimes_intervention_html_source(
            download_html, html_file_path
        )
        if download_html:
            csv_file_path = _DEFAULT_CSV_PATH
        else:
            accessed = _ACCESSED_.format(DATE=_get_access_date(html_file_path))
            csv_file_path = _BASE_DIR + _CSV_TAG + accessed + ".csv"

        interv_df = parse_intervention_soup(
            soup, csv_file_path, verbose=verbose
        )
        interv_df["effective_on"] = pd.to_datetime(interv_df.effective_on)
    return interv_df


# # parse_interventions.py ends here
