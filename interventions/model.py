import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression as _LR
import statsmodels.api as _sm

import data


def _fit_OLS(df_, xvar="shifted_day", yvar="confirmed_cases"):
    """
    _fit_OLS(df_, xvar="shifted_day", yvar="confirmed_cases")

    Assumes that xvar is a pd.Datetime object of some kind.
    """
    X = df_[xvar].dt.days.values.reshape(-1, 1)
    y = np.log(df_[yvar].values.ravel())
    X = _sm.add_constant(X)
    fitted_model = _sm.OLS(y, X).fit()

    return fitted_model


def _fit_models_on_date_segments(
    df,
    state,
    dates,
    column_name="confirmed_cases",
    obs_threshold=5,
    verbose=True,
):
    if dates is None:
        raise
    elif np.iterable(dates):
        date0, date1 = dates
    else:
        date0, date1 = dates, dates
    # interpret date1 as a lag from date0 if it's not a pd.datetime object
    if isinstance(date1, (np.int, np.float)):
        date1 = pd.Timedelta(date1, "day")
    if isinstance(date1, pd.Timedelta):
        date1 = date0 + date1

    state_df = data.subset_by_state(df, state).sort_values(
        ["Province_State", "shifted_day"]
    )

    df0 = state_df.loc[state_df.Date <= date0]
    df1 = state_df.loc[state_df.Date >= date1]

    model0, model1 = None, None
    if df0.shape[0] >= obs_threshold:
        model0 = _fit_OLS(df0, "shifted_day", column_name)

    if df1.shape[0] >= obs_threshold:
        model1 = _fit_OLS(df1, "shifted_day", column_name)

    return model0, model1


def _fit_models_on_thirds(
    df, state, column_name="confirmed_cases", verbose=True
):
    """
    fit_models_on_thirds(
        df, state, column_name="confirmed_cases", verbose=True
    )

    Inputs
    ------
    df : pd.DataFrame
        A dataframe with the columns:
            "Province_State", "shifted_day" and `column_name`.
    state : string
        One of the entries in df.Province_State.unique()
    column_name : string
        Likely either "confirmed_cases" or "deaths".
    verbose : bool
        Whether to include verbose output of model fitting.
    """
    state_df = data.subset_by_state(df, state).sort_values(
        ["Province_State", "shifted_day"]
    )
    record_length = state_df.shape[0]

    lm0 = _LR(normalize=True)
    lm1 = _LR(normalize=True)

    num_events = record_length // 3

    init_data = state_df.iloc[:num_events, :]
    final_data = state_df.iloc[-num_events:, :]
    X_init = init_data.shifted_day.dt.days.values.reshape(-1, 1)
    y_init = np.log(init_data[column_name].values.ravel())
    X_final = final_data.shifted_day.dt.days.values.reshape(-1, 1)
    y_final = np.log(final_data[column_name].values.ravel())

    lm0.fit(X_init, y_init)
    lm1.fit(X_final, y_final)

    if verbose:
        print(f"Fit {column_name} for {state}:")
        print(f"lm0.coef_: {lm0.coef_[0]:.4f}")
        print(f"lm1.coef_: {lm1.coef_[0]:.4f}")

    return lm0, lm1


def fit_models_on_date_segments(
        df, column_name="confirmed_cases", interv_df, verbose=True
):
    """
    fit_models_on_date_segments(df, column_name="confirmed_cases", verbose=True)

    
    """
    results = {}
    for state in df.Province_State.unique():
        if state in interv_df.state_name.values
        date0 = 
    return


def fit_models_on_thirds(df, column_name="confirmed_cases", verbose=True):
    """
    fit_models_on_thirds(df, column_name="confirmed_cases", verbose=True)

    For each state, fit a linear model to the log-scaled data:
        (shifted_day, log(confirmed_cases))
    for the "initial" and "final" sections of the curve, respectively.

    Initial and final sections are determined by taking the first and last third
    of the series, respectively. Only series with more than 10 observations are
    considered.

    Inputs
    ------
    df : pd.DataFrame
    column_name : string
    verbose : bool

    Outputs
    -------
    lm_dict : dict of tuples
        Keys are the State names; each value is a 2-tuple of linear models,
        corresponding to the init and final fit, respectively.
    coef_df : pd.DataFrame
        A dataframe with columns: Province_State, coef_init, coef_final.
    """
    lm_dict = {
        state: _fit_models_on_thirds(df, state, column_name, verbose)
        for state in df.Province_State.unique()
    }

    coef_df = pd.DataFrame(
        [
            (key, value[0].coef_[0], value[1].coef_[0])
            for key, value in lm_dict.items()
        ],
        columns=["Province_State", "coef_init", "coef_final"],
    )
    return lm_dict, coef_df


def _get_pred_df(df, lm_dict, kind):
    kind = kind.lower()
    if kind not in ["init", "final"]:
        print(
            f"Warning: Expected kind to be either 'init' or 'final' but got {kind}"
        )
    elif kind == "init":
        idx = 0
    elif kind == "final":
        idx = 1
    else:
        raise RuntimeError("This could not have been foreseen.")

    pred_df = (
        df.groupby("Province_State")
        .apply(
            lambda group: np.exp(
                lm_dict[group.name][idx]
                .predict(group.shifted_day_value.values.reshape(-1, 1))
                .ravel()
            )
        )
        .explode()
        .reset_index()
        .rename(columns={0: f"pred_{kind}"})
    )
    return pred_df


def predict_on_shifted_day(df, lm_dict):
    if "shifted_day_value" not in df.columns:
        df["shifted_day_value"] = df.shifted_day.dt.days

    # Make sure everything's in the right order
    df = df.sort_values(["Province_State", "shifted_day_value"]).reset_index(
        drop=True
    )

    # Get prediction for each group
    pred_init_df = _get_pred_df(df, lm_dict, "init")
    pred_final_df = _get_pred_df(df, lm_dict, "final")

    # Make sure pred_df has access to the domain so it can be joined
    pred_init_df["shifted_day_value"] = df.shifted_day_value
    pred_final_df["shifted_day_value"] = df.shifted_day_value

    # Do the join
    pred_df = pred_init_df.set_index(
        ["Province_State", "shifted_day_value"]
    ).join(pred_final_df.set_index(["Province_State", "shifted_day_value"]))

    df = (
        df.set_index(["Province_State", "shifted_day_value"])
        .join(pred_df)
        .reset_index()
    )
    return df


def print_lm_summary(coef_case, coef_death, partial=True):
    """
    print_partial_lm_summary(coef_case, coef_death)

    Only prints information for those States for which coefs are available for
    both cases and deaths. This just helps keep things from looking too messy.
    """
    lm_summary = (
        coef_case.set_index("Province_State")
        .join(
            coef_death.set_index("Province_State"),
            lsuffix="_case",
            rsuffix="_death",
        )
        .reset_index()
    )
    if partial:
        lm_summary = lm_summary.dropna()
    return lm_summary


def get_shifted_day_for_interventions(interv_df, events_df):
    """
    get_shifted_day_for_interventions(interv_df, events_df)

    Inputs
    ------
    interv_df : pd.DataFrame
        e.g., like what is returned by load_formatted_us_intervention_data
    events_df : pd.DataFrame
        e.g., like what is returned by load_us_state_data

    Output
    ------
    interv_df_with_shifted_day : pd.DataFrame
        Has columns:
            state_name, state_code,   population,
            order_type, effective_on, shifted_day,
            shifted_day_value

    """
    state_wide = interv_df.loc[interv_df.state_wide]
    non_state_wide = interv_df.loc[~interv_df.state_wide]

    non_state_wide_agg = (
        non_state_wide.groupby(["state_name"])
        .apply(
            lambda group: pd.DataFrame(
                {
                    "state_code": [group.state_code.iloc[0]],
                    "population": [group.population.sum()],
                    "order_type": [pd.value_counts(group.order_type).index[0]],
                    "effective_on": [group.effective_on.mean()],
                }
            )
        )
        .reset_index(level=0)
        .reset_index(drop=True)
    )

    ordinances_agg = (
        pd.concat((state_wide, non_state_wide_agg))
        .sort_values(["effective_on"])
        .reset_index(drop=True)
    )

    day_shift_df = events_df.loc[events_df.shifted_day_value == 0][
        ["Province_State", "shifted_day_value", "Date"]
    ]

    interv_df_with_shifted_day = (
        ordinances_agg[
            [
                "state_name",
                "state_code",
                "population",
                "order_type",
                "effective_on",
            ]
        ]
        .set_index("state_name")
        .join(day_shift_df.set_index("Province_State"))
        .reset_index()
    )

    interv_df_with_shifted_day["shifted_day"] = (
        interv_df_with_shifted_day.effective_on
        - interv_df_with_shifted_day.Date
    )

    interv_df_with_shifted_day[
        "shifted_day_value"
    ] = interv_df_with_shifted_day.shifted_day.dt.days

    interv_df_with_shifted_day = interv_df_with_shifted_day.dropna()[
        [
            "state_name",
            "state_code",
            "population",
            "order_type",
            "effective_on",
            "shifted_day",
            "shifted_day_value",
        ]
    ]
    return interv_df_with_shifted_day


if __name__ == "__main__":
    confirmed, df = data.load_us_state_data()
    lm_dict_confirmed, coef_confirmed = fit_models_on_thirds(confirmed)
