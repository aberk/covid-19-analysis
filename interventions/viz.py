import matplotlib.pyplot as plt
import seaborn as sns
from data import subset_by_state


def plot_response_by_shifted_day(
    df, value_name="confirmed_cases", figsize=(12, 6), **kwargs
):
    """
    plot_response_by_shifted_day(
        df, value_name="confirmed_cases", figsize=(15, 7)
    )

    reshapes df and modifies the resulting column names so that it can be
    plotted with pd.DataFrame.plot.

    Output
    ------
    ax : plt.Axes object
        The axis object returned by pd.DataFrame.plot
    """
    df_plottable = df[
        ["Province_State", "shifted_day", value_name]
    ].pivot_table(index="shifted_day", columns="Province_State")
    df_plottable.columns = df_plottable.columns.get_level_values(1)
    ax = df_plottable.plot.line(legend=True, figsize=figsize, **kwargs)
    plt.yscale("log")
    plt.legend(ncol=2, loc="upper left", bbox_to_anchor=(1, 1))
    plt.tight_layout()
    return ax


def plot_pred_on_thirds(
    df,
    value_name="confirmed_cases",
    ax=None,
    figsize=(12, 6),
    use_subplots=False,
    **kwargs,
):
    cpal = sns.categorical.color_palette(
        n_colors=df.Province_State.unique().size
    )

    alpha = kwargs.pop("alpha", 0.5)
    yLabel = kwargs.pop("yLabel", value_name)
    xLabel = kwargs.pop("xLabel", "Shifted Day")

    if (ax is None) and not use_subplots:
        ax_was_none = True
        fig, ax = plt.subplots(1, 1, figsize=figsize)
    elif not use_subplots:
        ax_was_none = False
    else:
        ax_was_none = True
        ncol = kwargs.pop("ncol", 4)
        nthings = df.Province_State.unique().size
        nrow = int(nthings / ncol) + 1
        fig, axx = plt.subplots(
            nrow, ncol, sharex=True, sharey=True, figsize=(3 * ncol, 3 * nrow)
        )
        axx = axx.ravel()

    for i, (group_name, group) in enumerate(df.groupby("Province_State")):
        if use_subplots:
            ax = axx[i]
        ax.plot(
            group.shifted_day_value.values,
            group[value_name].values,
            "-",
            alpha=alpha,
            label=group_name,
            c=cpal[i],
        )
        total_pts = group.shifted_day_value.size
        n_pts = total_pts // 2
        ax.plot(
            group.shifted_day_value[:n_pts],
            group.pred_init[:n_pts],
            ":",
            c=cpal[i],
        )
        ax.plot(
            group.shifted_day_value[-n_pts:],
            group.pred_final[-n_pts:],
            ":",
            c=cpal[i],
        )
        if use_subplots:
            ax.set_yscale("log")
            ax.set_title(group_name)
            if (i % 4) == 0:
                ax.set_ylabel(yLabel)
            if (nrow * ncol - i) <= 4:
                ax.set_xlabel(xLabel)

    if not use_subplots:
        ax.legend(loc="upper left", bbox_to_anchor=(1, 1))
        ax.set_yscale("log")
        ax.set_xlabel(xLabel)
        ax.set_ylabel(yLabel)
    else:
        for i in range(nthings, nrow * ncol):
            axx[i].axis("off")

    if ax_was_none:
        try:
            return fig, axx
        except Exception:
            return fig, ax
    return


def add_ordinance_vlines_to_axes(
    axx, interv_df, events_df=None, event_name="confirmed_cases"
):
    for ax in axx:
        state_name = ax.title.get_text()
        if state_name in interv_df.state_name.values:
            if events_df is None:
                y0, y1 = ax.get_ylim()
            else:
                y0, y1 = (
                    subset_by_state(events_df, state_name)[event_name]
                    .agg(["min", "max"])
                    .values
                )
            ax.vlines(
                interv_df.loc[
                    interv_df.state_name == state_name
                ].shifted_day_value,
                y0,
                y1,
                linestyle="dashed",
                linewidth=1,
            )
    return axx
